package oop.ex4.data_structures;

//TODO add a class description
public class Node {

    /*----=  magic numbers  =-----*/

    /**
     * The value the node contains
     */
    private int value;

    /**
     * The father of the node
     */
    private Node father;

    /**
     * The right son of the node (with a bigger value than the node)
     */
    private Node rightSon;

    /**
     * The left son of the node (with a smaller value than the node)
     */
    private Node leftSon;

    /**
     * The depth of node which is the distance of the node from the root.
     */
    private int depth;

    /**
     * The height of a node, measured as the longest path from the leaf to the node.
     */
    private int height;


    /**
     * The depth of the root
     */
    private static final int ROOT_DEPTH = 0;

    /**
     * The height of a leaf
     */
    private static final int LEAF_HEIGHT = 0;

    /*----=  constructor  =-----*/

    /**
     * Creates a new node with the given values
     *
     * @param value  The value the node contains
     * @param father The father of the node
     */
    public Node(int value, Node father) {
        this.value = value;
        this.father = father;
        this.height = LEAF_HEIGHT;
        if (father != null) {
            this.depth = father.depth + 1;
        } else {
            this.depth = ROOT_DEPTH;
        }
    }

    /*----=  getters  =-----*/

    /**
     * @return value of the node
     */
    int getValue() {
        return value;
    }

    /**
     * @return father of the node
     */
    Node getFather() {
        return father;
    }

    /**
     * @return right son of the node
     */
    Node getRightSon() {
        return rightSon;
    }

    /**
     * @return left son of the node
     */
    Node getLeftSon() {
        return leftSon;
    }

    /**
     * @return depth of the node
     */
    int getDepth() {
        return depth;
    }

    /**
     * @return height of the node
     */
    int getHeight() {
        return height;
    }

    /*----=  setters  =-----*/

    /**
     * sets the value of the node
     *
     * @param value new value to be set
     */
    void setValue(int value) {
        this.value = value;
    }

    /**
     * sets the father of the node
     *
     * @param father new father to be set
     */
    void setFather(Node father) {
        this.father = father;
    }

    /**
     * sets the right son of the node
     *
     * @param rightSon new right son to be set
     */
    void setRightSon(Node rightSon) {
        this.rightSon = rightSon;
    }

    /**
     * sets the left son of the node
     *
     * @param leftSon new left son to be set
     */
    void setLeftSon(Node leftSon) {
        this.leftSon = leftSon;
    }

    /**
     * sets the depth of the node
     *
     * @param depth new depth to be set
     */
    void setDepth(int depth) {
        this.depth = depth;
    }

    /**
     * sets the height of the node
     *
     * @param height new height to be set
     */
    void setHeight(int height) {
        this.height = height;
    }
}