package oop.ex4.data_structures;

import java.util.Iterator;

//TODO add a class description
public class AvlTree extends BinarySearchTree {

    /*----=  magic numbers  =-----*/

    /**
     * The height of null node
     */
    private static final int NULL_HEIGHT = -1;
//TODO think of more descriptive names - I'm not managing to...!
    /**
     * balance factor of the node is 2 - weighed down on left.
     */
    private static final int BALANCE_FACTOR_2 = 2;

    /**
     * balance factor of the node is 1 - weighed down on left.
     */
    private static final int BALANCE_FACTOR_1 = 1;

    /**
     * balance factor of the node is 0 - balanced.
     */
    private static final int NODE_BALANCED = 0;

    /**
     * balance factor of the node is -1 - weighed down on right.
     */
    private static final int BALANCE_FACTOR_neg1 = -1;

    /**
     * balance factor of the node is -2 - weighed down on right.
     */
    private static final int BALANCE_FACTOR_neg2 = -2;

    /*----=  Constructors  =-----*/

    public AvlTree() {
        super();
    }

    public AvlTree(AvlTree tree) {
        super(tree);
    }

    public AvlTree(int[] data) {
        super(data);
    }

    /*----=  Balancing tree methods  =-----*/

    /**
     * A rotation made on an unbalanced node, where the right-sub tree of the node is of a greater height
     * than the height of the left sub-tree of the node.
     *
     * @param unbalancedNode the unbalanced node (the node that violates the avl property)
     */
    private void rotateLeft(Node unbalancedNode) {
        Node newRoot = unbalancedNode.getRightSon();
        Node father = unbalancedNode.getFather();
        Node newNodeLeft = newRoot.getLeftSon();

        newRoot.setFather(father);
        if (father != null) {

            if (unbalancedNode.getValue() > unbalancedNode.getFather().getValue()) { //node is fathers right son
                father.setRightSon(newRoot);
            } else { // node is fathers left son
                father.setLeftSon(newRoot);
            }
        } else {
            root = newRoot;
        }
        unbalancedNode.setRightSon(newNodeLeft); //could be null
        if (newNodeLeft != null) { // newRoot has a left son, if we were to just rotate, we would lose him
            newNodeLeft.setFather(unbalancedNode);
        }
        newRoot.setLeftSon(unbalancedNode);
        unbalancedNode.setFather(newRoot);
        updateHeight(unbalancedNode);
        updateDepth(newRoot);
    }

    /**
     * A rotation made on an unbalanced node, where the left-sub tree of the node is of a greater height
     * than the height of the right sub-tree of the node.
     *
     * @param unbalancedNode the unbalanced node (the node that violates the avl property)
     */
    private void rotateRight(Node unbalancedNode) {
        Node newRoot = unbalancedNode.getLeftSon();
        Node father = unbalancedNode.getFather();
        Node newNodeRight = newRoot.getRightSon();

        newRoot.setFather(father);
        if (father != null) {
            if (unbalancedNode.getValue() > unbalancedNode.getFather().getValue()) { //node is fathers right son
                father.setRightSon(newRoot);
            } else { // node is fathers left son
                father.setLeftSon(newRoot);
            }
        } else {
            root = newRoot;
        }
        unbalancedNode.setLeftSon(newNodeRight); //could be null
        if (newNodeRight != null) { // newRoot has a right son, if we were to just rotate, we would lose him
            newNodeRight.setFather(unbalancedNode);
        }
        newRoot.setRightSon(unbalancedNode);
        unbalancedNode.setFather(newRoot);
        updateHeight(unbalancedNode);
        updateDepth(newRoot);
    }

    /**
     * finds the balance factor of the Node given.
     *
     * @param node the node we are checking
     * @return the balance factor.
     */
    private int findBalanceFactor(Node node) {
        int rightSubTreeHeight = NULL_HEIGHT;
        int leftSubTreeHeight = NULL_HEIGHT;

        if (node == null) {
            return NODE_BALANCED;
        }

        if (node.getRightSon() != null) {
            rightSubTreeHeight = node.getRightSon().getHeight();
        }
        if (node.getLeftSon() != null) {
            leftSubTreeHeight = node.getLeftSon().getHeight();
        }
        return leftSubTreeHeight - rightSubTreeHeight;
    }

    /**
     * method finds what violation occurred and fixes it
     *
     * @param violatedNode The node where the violation occurred
     */
    private void balanceAvlTree(Node violatedNode) {
        int violatedNodeBalance = findBalanceFactor(violatedNode);
        int leftSonBalance = findBalanceFactor(violatedNode.getLeftSon());
        int rightSonBalance = findBalanceFactor(violatedNode.getRightSon());

        if (violatedNodeBalance == BALANCE_FACTOR_2 &&
                (leftSonBalance == NODE_BALANCED || leftSonBalance == BALANCE_FACTOR_1)) {//LL violation
            rotateRight(violatedNode);

        } else if (violatedNodeBalance == BALANCE_FACTOR_2 && leftSonBalance == BALANCE_FACTOR_neg1) { //LR violation
            rotateLeft(violatedNode.getLeftSon());
            rotateRight(violatedNode);

        } else if (violatedNodeBalance == BALANCE_FACTOR_neg2 &&
                (rightSonBalance == NODE_BALANCED || rightSonBalance == BALANCE_FACTOR_neg1)) { //RR violation
            rotateLeft(violatedNode);
        } else if (violatedNodeBalance == BALANCE_FACTOR_neg2 && rightSonBalance == BALANCE_FACTOR_1) { //RL violation
            rotateRight(violatedNode.getRightSon());
            rotateLeft(violatedNode);
        }
    }

    @Override
        //TODO not sure if should have a different description + 1 should be magic number?
    void fixTreeAfterChange(Node node) {
        if (Math.abs(findBalanceFactor(node)) > 1) {
            balanceAvlTree(node);
            return;

        } else if (node.getFather() == null) {
            return;
        }
        fixTreeAfterChange(node.getFather());
    }

    /*----=  Static methods  =-----*/

    //TODO also not sure if numbers here should be magic - I don't think so...

    /**
     * A method that calculates the minimum numbers of nodes in an AVL tree of height h.
     *
     * @param h height of the tree (a non-negative number).
     * @return minimum number of nodes of height h
     */
    public static int findMinNodes(int h) {
        return findFibonacciNumber(h + 3) - 1;
    }

    /**
     * A method that calculates the maximum numbers of nodes in an AVL tree of height h.
     *
     * @param h height of the tree (a non-negative number).
     * @return maximum number of nodes of height h
     */
    public static int findMaxNodes(int h) {
        return (int) Math.pow(2, h + 1) - 1;
    }

    /**
     * method finds the n'th fibonacci number
     *
     * @param n the fibonacci number were looking for
     * @return the n'th fibonacci number
     */
    private static int findFibonacciNumber(int n) {
        double fibNumber = (1 / Math.sqrt(5)) * (Math.pow(((1 + Math.sqrt(5)) / 2), n) - Math.pow(((1 - Math.sqrt(5)) / 2), n));
        return (int) fibNumber;
    }
}