package oop.ex4.data_structures;

public class ThirdTester {

    public static void main(String[] args) {
//        bensRandomness();
        evyatarTree_17();
        minSize();

    }

    public static void minSize(){
        System.out.println(AvlTree.findMinNodes(4));
    }

    public static void evyatarTree_17(){
        int[] add = new int[]{1, 2, 3, 4, 5, 6, 0, 0, 1, 2, 3, 4, 5, 6};
        AvlTree avl = new AvlTree(add);
        System.out.println(avl.delete(0));
        System.out.println(avl.delete(1));

    }

    public static void bensRandomness() {
        AvlTree avlTree = new AvlTree();
        avlTree.add(81);
        avlTree.add(62);
        avlTree.add(57);
        avlTree.add(77);
        avlTree.add(96);
        avlTree.add(56);
        avlTree.add(99);
        avlTree.add(23);
        avlTree.add(79);
        avlTree.add(32);
        avlTree.add(53);
        avlTree.add(15);
        avlTree.add(18);
        avlTree.add(26);
        avlTree.add(75);
        avlTree.add(12);
        avlTree.add(22);
        avlTree.add(8);
        avlTree.add(89);
        avlTree.add(63);
        avlTree.add(73);
        avlTree.add(66);
        avlTree.add(85);
        avlTree.add(21);
        avlTree.add(36);
        avlTree.add(35);
        avlTree.add(25);
        avlTree.add(42);
        avlTree.add(39);
        avlTree.add(37);
        avlTree.add(71);
        avlTree.add(45);
        avlTree.add(83);
        avlTree.add(19);
        avlTree.add(5);
        avlTree.add(51);
        avlTree.add(78);
        avlTree.add(13);
        avlTree.add(3);
        avlTree.add(48);
        avlTree.add(58);
        avlTree.add(54);
        avlTree.add(72);
        avlTree.add(24);
        avlTree.add(91);
        avlTree.add(52);
        avlTree.add(2);
        avlTree.add(41);
        avlTree.add(33);
        avlTree.add(100);
        avlTree.add(93);
        avlTree.add(95);
        avlTree.add(4);
        avlTree.add(74);
        avlTree.add(65);
        avlTree.add(92);
        avlTree.add(80);
        avlTree.add(300);
        avlTree.add(399);
        avlTree.add(299);
        avlTree.add(199);
        avlTree.add(195);
        avlTree.add(150);
        avlTree.add(151);
        avlTree.add(152);
        avlTree.add(153);
        avlTree.add(301);
        avlTree.add(311);
        avlTree.add(312);
        avlTree.add(380);
        avlTree.add(149);
        avlTree.add(148);
//_size_72
        System.out.println("before deleting");
        System.out.println(avlTree.contains(65));
        avlTree.delete(81);
        System.out.println("after deleting 81");
        System.out.println(avlTree.contains(65));
        avlTree.delete(62);
        System.out.println("after deleting 62");
        System.out.println(avlTree.returnNode(83).getFather().getValue() == 63);
        System.out.println(avlTree.returnNode(66).getLeftSon().getValue() == 65);
        System.out.println(avlTree.contains(65));
        avlTree.delete(57);
        avlTree.delete(77);
//_size_68
        System.out.println("next tab");
        System.out.println(avlTree.contains(65));
        avlTree.delete(96);
        avlTree.delete(56);
        avlTree.delete(99);
        avlTree.delete(23);
        avlTree.delete(79);
        avlTree.delete(32);
        avlTree.delete(53);
        avlTree.delete(15);
//_size_60
        avlTree.delete(18);
        avlTree.delete(26);
        avlTree.delete(75);
        avlTree.delete(12);
        avlTree.delete(22);
        avlTree.delete(41);
        avlTree.delete(100);
        avlTree.delete(95);
        avlTree.delete(93);
        avlTree.delete(80);
//_size_50
        avlTree.delete(83);
//_size_49
        System.out.println(avlTree.contains(65));
    }
}
