package oop.ex4.data_structures;


public class Tester {

    public static void main(String[] args) {
        firstTester();
        secondTester();
//        testingLeftRotation();
//        testingRightRotation();
//        test_6_findMinNodes();
//        test_7_findMaxNodes();
//        testerAvl();
//        testDepth();

    }

    public static void testDepth() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.add(5);
        binarySearchTree.add(3);
        binarySearchTree.add(4);
        binarySearchTree.add(2);
        binarySearchTree.add(-1);
        binarySearchTree.add(1);
        binarySearchTree.add(-2);
        //checking heights initiated correctly
        System.out.println(binarySearchTree.returnNode(5).getDepth() == 0);
        System.out.println(binarySearchTree.returnNode(3).getDepth() == 1);
        System.out.println(binarySearchTree.returnNode(4).getDepth() == 2);
        System.out.println(binarySearchTree.returnNode(2).getDepth() == 2);
        System.out.println(binarySearchTree.returnNode(-1).getDepth() == 3);
        System.out.println(binarySearchTree.returnNode(-2).getDepth() == 4);
        System.out.println(binarySearchTree.returnNode(1).getDepth() == 4);


        binarySearchTree.delete(5);
        //checking heights were updated correctly
        System.out.println(binarySearchTree.returnNode(3).getDepth() == 0);
        System.out.println(binarySearchTree.returnNode(4).getDepth() == 1);
        System.out.println(binarySearchTree.returnNode(2).getDepth() == 1);
        System.out.println(binarySearchTree.returnNode(-1).getDepth() == 2);
        System.out.println(binarySearchTree.returnNode(-2).getDepth() == 3);
        System.out.println(binarySearchTree.returnNode(1).getDepth() == 3);

        // AVL tree
        AvlTree avlTree = new AvlTree();
        avlTree.add(10);
        avlTree.add(20);
        avlTree.add(30);
        System.out.println(avlTree.returnNode(10).getDepth() == 1);
        System.out.println(avlTree.returnNode(20).getDepth() == 0);
        System.out.println(avlTree.returnNode(30).getDepth() == 1);
        avlTree.add(40);
        avlTree.add(50);
        System.out.println(avlTree.returnNode(10).getDepth() == 1);
        System.out.println(avlTree.returnNode(20).getDepth() == 0);
        System.out.println(avlTree.returnNode(30).getDepth() == 2);
        System.out.println(avlTree.returnNode(40).getDepth() == 1);
        System.out.println(avlTree.returnNode(50).getDepth() == 2);
        avlTree.add(60);
        System.out.println(avlTree.returnNode(10).getDepth() == 2);
        System.out.println(avlTree.returnNode(20).getDepth() == 1);
        System.out.println(avlTree.returnNode(30).getDepth() == 2);
        System.out.println(avlTree.returnNode(40).getDepth() == 0);
        System.out.println(avlTree.returnNode(50).getDepth() == 1);
        System.out.println(avlTree.returnNode(60).getDepth() == 2);




    }

    public static void testerAvl() {
        AvlTree avlTree = new AvlTree();
        avlTree.add(20);
        avlTree.add(30);
        avlTree.add(40);
        System.out.println(avlTree.returnNode(30).getRightSon().getValue() == 40);
        System.out.println(avlTree.returnNode(30).getLeftSon().getValue() == 20);
    }

    public static void testingLeftRotation() {
        AvlTree avlTree = new AvlTree();
        avlTree.add(6);
        avlTree.add(8);
        avlTree.add(9);
        avlTree.add(10);


        //Testing that the creation of the AVL tree was correct
        System.out.println(avlTree.root.getValue() == 6);
        System.out.println(avlTree.root.getLeftSon() == null);
        System.out.println(avlTree.root.getRightSon().getValue() == 8);
        System.out.println(avlTree.root.getRightSon().getLeftSon() == null);
        System.out.println(avlTree.root.getRightSon().getRightSon().getValue() == 9);
        System.out.println(avlTree.root.getRightSon().getRightSon().getRightSon().getValue() == 10);
        System.out.println(avlTree.root.getRightSon().getRightSon().getLeftSon() == null);
        System.out.println(avlTree.root.getHeight() == 3);


        /**
         * Testing the rotate left move
         */
//        avlTree.rotateLeft(avlTree.root.getRightSon()); //rotated around 8

        //testing that all the pointers moved correctly
        System.out.println("\n Basic checks");
        System.out.println(avlTree.root.getValue() == 6);
        System.out.println(avlTree.root.getLeftSon() == null);
        System.out.println(avlTree.root.getRightSon().getValue() == 9);
        System.out.println(avlTree.root.getRightSon().getLeftSon().getValue() == 8);
        System.out.println(avlTree.root.getRightSon().getRightSon().getValue() == 10);
        System.out.println(avlTree.root.getRightSon().getRightSon().getRightSon() == null);
        System.out.println(avlTree.root.getRightSon().getRightSon().getLeftSon() == null);
        System.out.println(avlTree.root.getHeight() == 2); // will return false since we need to first update
        // the height


        System.out.println("\n More checks");
        System.out.println(avlTree.returnNode(6).getRightSon().getValue() == 9);
        System.out.println(avlTree.returnNode(9).getRightSon().getValue() == 10);
        System.out.println(avlTree.returnNode(8).getFather().getValue() == 9);
        System.out.println(avlTree.returnNode(9).getFather().getValue() == 6);


        //Building a tree with only three nodes
        System.out.println("Another tree");
        AvlTree newAvlTree = new AvlTree();
        newAvlTree.add(6);
        newAvlTree.add(8);
        newAvlTree.add(9);

        //testing the build of the tree
        System.out.println(newAvlTree.root.getValue() == 6);
        System.out.println(newAvlTree.returnNode(9).getFather().getValue() == 8);

//        newAvlTree.rotateLeft(newAvlTree.returnNode(6));

        //The son has 2 sons
        System.out.println("slightly more complicated tree");
        AvlTree fullAvlTree = new AvlTree();
        //building the tree
        fullAvlTree.add(10);
        fullAvlTree.add(15);
        fullAvlTree.add(13);
        fullAvlTree.add(12);
        fullAvlTree.add(14);
        fullAvlTree.add(20);
        fullAvlTree.add(17);
        fullAvlTree.add(25);

        //making sure right subtree built properly
        System.out.println(fullAvlTree.returnNode(25).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(17).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(20).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getFather().getValue() == 10);
        System.out.println(fullAvlTree.returnNode(10).getRightSon().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getRightSon().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(20).getRightSon().getValue() == 25);
        System.out.println(fullAvlTree.returnNode(20).getLeftSon().getValue() == 17);

        //making sure right subtree built properly
        System.out.println(fullAvlTree.returnNode(12).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(14).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getLeftSon().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getLeftSon().getValue() == 12);
        System.out.println(fullAvlTree.returnNode(13).getRightSon().getValue() == 14);

        //checking left rotation around 15
//        fullAvlTree.rotateLeft(fullAvlTree.returnNode(15));
        //making sure right subtree built properly
        System.out.println(fullAvlTree.returnNode(25).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(17).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(20).getFather().getValue() == 10);
        System.out.println(fullAvlTree.returnNode(15).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(10).getRightSon().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(20).getRightSon().getValue() == 25);
        System.out.println(fullAvlTree.returnNode(20).getLeftSon().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getRightSon().getValue() == 17);
        System.out.println(fullAvlTree.returnNode(15).getLeftSon().getValue() == 13);


        //making sure right subtree built properly - nothing should have changed here
        System.out.println(fullAvlTree.returnNode(12).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(14).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getLeftSon().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getLeftSon().getValue() == 12);
        System.out.println(fullAvlTree.returnNode(13).getRightSon().getValue() == 14);

    }

    public static void testingRightRotation() {

        AvlTree newAvlTree = new AvlTree();
        newAvlTree.add(8);
        newAvlTree.add(6);
        newAvlTree.add(4);
        newAvlTree.add(3);

        //Checks to make sure the tree was constructed correctly
        System.out.println("Testing building of rotate right tree");
        System.out.println(newAvlTree.root.getValue() == 8);
        System.out.println(newAvlTree.returnNode(8).getRightSon() == null);
        System.out.println(newAvlTree.returnNode(8).getLeftSon().getValue() == 6);
        System.out.println(newAvlTree.returnNode(6).getLeftSon().getValue() == 4);
        System.out.println(newAvlTree.returnNode(3).getFather().getValue() == 4);
        System.out.println(newAvlTree.returnNode(8).getHeight() == 3);

//        newAvlTree.rotateRight(newAvlTree.returnNode(6));

        System.out.println("After rotation");
        System.out.println(newAvlTree.root.getValue() == 8);
        System.out.println(newAvlTree.returnNode(8).getRightSon() == null);
        System.out.println(newAvlTree.returnNode(8).getLeftSon().getValue() == 4);
        System.out.println(newAvlTree.returnNode(4).getFather().getValue() == 8);
        System.out.println(newAvlTree.returnNode(6).getLeftSon() == null);
        System.out.println(newAvlTree.returnNode(6).getRightSon() == null);
        System.out.println(newAvlTree.returnNode(3).getFather().getValue() == 4);
        System.out.println(newAvlTree.returnNode(8).getHeight() == 2); //will return false since didn't
        // yet update height


        //The son has 2 sons
        System.out.println("slightly more complicated tree");
        AvlTree fullAvlTree = new AvlTree();
        //building the tree
        fullAvlTree.add(6);
        fullAvlTree.add(7);
        fullAvlTree.add(8);
        fullAvlTree.add(9);
        fullAvlTree.add(10);
        fullAvlTree.add(15);
        fullAvlTree.add(13);
        fullAvlTree.add(12);
        fullAvlTree.add(14);
        fullAvlTree.add(20);
        fullAvlTree.add(17);
        fullAvlTree.add(25);

        //making sure right subtree built properly
        System.out.println(fullAvlTree.returnNode(25).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(17).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(20).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getFather().getValue() == 10);
        System.out.println(fullAvlTree.returnNode(10).getRightSon().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getRightSon().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(20).getRightSon().getValue() == 25);
        System.out.println(fullAvlTree.returnNode(20).getLeftSon().getValue() == 17);

        //making sure right subtree built properly
        System.out.println(fullAvlTree.returnNode(12).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(14).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getLeftSon().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getLeftSon().getValue() == 12);
        System.out.println(fullAvlTree.returnNode(13).getRightSon().getValue() == 14);

        //checking right rotation around 15
//        fullAvlTree.rotateRight(fullAvlTree.returnNode(15));
        System.out.println("tree has been rotated");
        //making sure built properly - fathers
        System.out.println(fullAvlTree.returnNode(25).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(17).getFather().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(20).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(14).getFather().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(15).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(12).getFather().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getFather().getValue() == 10);
        //making sure built properly - sons
        System.out.println(fullAvlTree.returnNode(10).getRightSon().getValue() == 13);
        System.out.println(fullAvlTree.returnNode(13).getRightSon().getValue() == 15);
        System.out.println(fullAvlTree.returnNode(13).getLeftSon().getValue() == 12);
        System.out.println(fullAvlTree.returnNode(15).getRightSon().getValue() == 20);
        System.out.println(fullAvlTree.returnNode(15).getLeftSon().getValue() == 14);
        System.out.println(fullAvlTree.returnNode(20).getRightSon().getValue() == 25);
        System.out.println(fullAvlTree.returnNode(20).getLeftSon().getValue() == 17);

        //checking heights
        System.out.println("height check");
        System.out.println(fullAvlTree.contains(17) == 0);
        System.out.println(fullAvlTree.contains(25) == 0);
        System.out.println(fullAvlTree.contains(12) == 0);
        System.out.println(fullAvlTree.contains(14) == 0);
        System.out.println(fullAvlTree.contains(20) == 1);
        System.out.println(fullAvlTree.contains(15) == 2);
        System.out.println(fullAvlTree.contains(13) == 3);
        System.out.println(fullAvlTree.contains(10) == 4);
        System.out.println(fullAvlTree.contains(9) == 5);
        System.out.println(fullAvlTree.contains(8) == 6);
        System.out.println(fullAvlTree.contains(7) == 7);
        System.out.println(fullAvlTree.contains(6) == 8);


//        System.out.println("\n Testing new tree");
//        AvlTree AvlTreeAgain = new AvlTree();
//        AvlTreeAgain.add(6);
//        AvlTreeAgain.add(4);
//        AvlTreeAgain.add(3);
//
//        System.out.println(AvlTreeAgain.root.getValue() == 6);
//        AvlTreeAgain.rotateRight(AvlTreeAgain.returnNode(6));
//        System.out.println(AvlTreeAgain.root.getValue()==4);


    }


    public static void secondTester() {
        //initiate root
//        oop.ex4.data_structures.Node rootNode = new oop.ex4.data_structures.Node();
//        rootNode.setValue(18);

        //initiate BAD DESIGN binary tree
        BinarySearchTree tree = new BinarySearchTree();
//        tree.root = rootNode;

        tree.add(18);
        System.out.println(tree.root.getHeight() == 0);
        tree.add(30);
        tree.add(10);
        tree.add(3);
        tree.add(16);
        tree.add(17);
//        System.out.println(tree.root.getHeight());
        System.out.println(tree.root.getLeftSon().getHeight() == 2);
        System.out.println(tree.root.getLeftSon().getRightSon().getValue() == 16);
        System.out.println(tree.root.getLeftSon().getRightSon().getRightSon().getValue() == 17);
        System.out.println(tree.root.getLeftSon().getRightSon().getLeftSon() == null);
        System.out.println(tree.root.getLeftSon().getLeftSon().getValue() == 3);
        System.out.println(tree.root.getLeftSon().getLeftSon().getHeight() == 0);

//        System.out.println(tree.delete(17));
//        System.out.println(tree.contains(16));

//        System.out.println(tree.delete(16));
//        System.out.println(tree.contains(17) == 0);
//        System.out.println(tree.contains(10) == 1);
//        System.out.println(tree.contains(18) == 2);

        tree.add(15);
        tree.add(14);
        tree.add(13);

        System.out.println(tree.delete(10));
        System.out.println(tree.contains(14) == 0);
        System.out.println(tree.contains(13) == 3);

//        System.out.println(tree.findSuccessor(tree.root.getLeftSon().getRightSon().getLeftSon().getLeftSon
//                ()).getValue() == 15);
//        System.out.println(tree.findSuccessor(tree.root).getValue() == 30);
//        System.out.println(tree.findSuccessor(tree.root.getLeftSon().getRightSon().getRightSon()).getValue
//                () == 18);


    }


    public static void firstTester() {
        System.out.println("first tester");

        //initiate root
//        oop.ex4.data_structures.Node rootNode = new oop.ex4.data_structures.Node();
//        rootNode.setValue(18);

        //initiate BAD DESIGN binary tree
        BinarySearchTree tree = new BinarySearchTree();
        tree.add(18);
//        tree.root = rootNode;

        //Add nodes to tree
        tree.add(-2); //left son
        tree.add(10); //right son
        tree.add(25);
        System.out.println("leftSon: " + tree.root.getLeftSon()); // should be -2
        System.out.println("leftSon: " + tree.root.getLeftSon()); // should be 10
        tree.add(50);
        tree.add(27);
        tree.add(3);
        tree.add(0);
        tree.add(26);
        tree.add(90);

        //entered values correctly on left tree
        System.out.println(18 == tree.root.getValue());
        System.out.println(-2 == tree.root.getLeftSon().getValue());
        System.out.println(null == tree.root.getLeftSon().getLeftSon());
        System.out.println(0 == tree.root.getLeftSon().getRightSon().getLeftSon().getLeftSon().getValue());

        //check height on left tree
        System.out.println(4 == tree.root.getLeftSon().getRightSon().getLeftSon().getLeftSon().getDepth());

        //entered values correctly on left tree
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getLeftSon().getValue() == 26);
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getFather().getValue() == 50);
        System.out.println(tree.root.getRightSon().getRightSon().getRightSon().getRightSon() == null);

        //check height on left tree
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getLeftSon().getDepth() == 4);

        //check contains, -1 = not contains
        System.out.println(tree.contains(3) == 3);
        System.out.println(tree.contains(4) == -1);
        System.out.println(tree.contains(26) == 4);
        System.out.println(tree.contains(90) == 3);
        System.out.println(tree.contains(18) == 0);
        System.out.println(tree.contains(2) == -1);
        System.out.println(tree.contains(28) == -1);

        System.out.println(" ");
        //checking deleting with one right son
        System.out.println("*******checking deleting with only one son*******");
        System.out.println("deleting 90");
        System.out.println(tree.delete(90));
        System.out.println(tree.root.getRightSon().getRightSon().getRightSon() == null);
        System.out.println(tree.contains(90) == -1);

        System.out.println("deleting 25");
        System.out.println(tree.delete(25));
        System.out.println(tree.root.getRightSon().getValue() == 50);
        System.out.println(tree.contains(25) == -1);


        System.out.println("deleting 3");
        System.out.println(tree.delete(3));
        System.out.println(tree.root.getLeftSon().getRightSon().getLeftSon().getValue() == 0);
        System.out.println(tree.contains(3) == -1);

        System.out.println("deleting 10");
        System.out.println(tree.delete(10));
        System.out.println(tree.root.getLeftSon().getRightSon().getValue() == 0);
        System.out.println(tree.contains(10) == -1);

        System.out.println("deleting non existing value");
        System.out.println(!tree.delete(100));

        tree.add(90);
        tree.add(80);
        tree.add(70);
        tree.add(60);
        System.out.println("");

//        System.out.println(tree.root.getValue());
//        System.out.println(tree.root.getRightSon().getLeftSon().getLeftSon().getLeftSon());

//        checking deleting with two sons
        System.out.println("checking deleting with two sons");

        System.out.println(tree.delete(50));

        System.out.println(tree.root.getRightSon().getValue() == 60);
        System.out.println("right subtree");
        //right subtree
        System.out.println(tree.root.getRightSon().getRightSon().getValue() == 90);
        System.out.println(tree.root.getRightSon().getRightSon().getFather().getValue() == 60);
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getValue() == 80);
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getLeftSon().getValue() == 70);
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getLeftSon().getLeftSon() == null);

        //left subtree
        System.out.println("left subtree");

        System.out.println(tree.root.getRightSon().getLeftSon().getValue() == 27);
        System.out.println(tree.root.getRightSon().getLeftSon().getFather().getValue() == 60);

        System.out.println(tree.root.getRightSon().getRightSon().getRightSon() == null);
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getValue() == 80);
        System.out.println(tree.root.getRightSon().getRightSon().getLeftSon().getLeftSon().getValue() == 70);

        tree.delete(70);
        tree.delete(80);

        tree.delete(60);
        System.out.println(tree.root.getRightSon().getValue() == 90);

        System.out.println(tree.root.getRightSon().getRightSon() == null);
        System.out.println(tree.root.getRightSon().getLeftSon().getValue() == 27);


        //delete root
        System.out.println("deleting root");
        System.out.println(tree.root.getRightSon().getLeftSon().getValue() == 27);
        System.out.println(tree.delete(18));
        System.out.println(tree.root.getValue() == 26);
        System.out.println(tree.root.getRightSon().getValue() == 90);
        System.out.println(tree.root.getRightSon().getRightSon() == null);
        System.out.println(tree.root.getRightSon().getLeftSon().getValue() == 27);
        System.out.println(tree.root.getLeftSon().getValue() == -2);

        System.out.println(tree.size());


    }

    public static void printNode(Node nodeToPrint) {
        System.out.println(nodeToPrint);
        System.out.println("left son " + nodeToPrint.getLeftSon() + " height: " + nodeToPrint.getLeftSon()
                .getDepth());
        System.out.println("right son " + nodeToPrint.getRightSon() + " height: " + nodeToPrint.getRightSon
                ().getDepth());

    }

    //evyatar min max nodes tests
    public static void test_6_findMinNodes() {
        System.out.println(AvlTree.findMinNodes(0) == 1);
        System.out.println(AvlTree.findMinNodes(1) == 2);
        System.out.println(AvlTree.findMinNodes(2) == 4);
        System.out.println(AvlTree.findMinNodes(3) == 7);
        System.out.println(AvlTree.findMinNodes(7) == 54);
        System.out.println(AvlTree.findMinNodes(16) == 4180);
        System.out.println(AvlTree.findMinNodes(25) == 317810);
        System.out.println(AvlTree.findMinNodes(40) == 433494436);
    }

    //    @Test
    public static void test_7_findMaxNodes() {
        System.out.println(AvlTree.findMaxNodes(0) == 1);
        System.out.println(AvlTree.findMaxNodes(1) == 3);
        System.out.println(AvlTree.findMaxNodes(2) == 7);
        System.out.println(AvlTree.findMaxNodes(3) == 15);
        System.out.println(AvlTree.findMaxNodes(7) == 255);
        System.out.println(AvlTree.findMaxNodes(16) == 131071);
        System.out.println(AvlTree.findMaxNodes(25) == 67108863);
    }


}
