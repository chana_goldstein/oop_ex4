package oop.ex4.data_structures;

import java.util.Iterator;

//TODO add a class description
public class BinarySearchTree implements Iterable<Integer> {

    /*----=  attributes  =-----*/

    /**
     * The root node of the BST
     */
    Node root;

    /**
     * A point to the last node found in the contains method.
     */
    private Node lastNodeFound;

    /**
     * Saves the size of the tree - the number of nodes.
     */
    private int size;

    /*----=  magic numbers  =-----*/

    /**
     * An int representing that a value is not in the binary tree
     */
    private static final int NOT_CONTAIN = -1;

    /**
     * The node does not have any sons
     */
    private static final int HAS_NO_SONS = 0;

    /**
     * The node only has a right son
     */
    private static final int ONLY_HAS_RIGHT_SON = 1;

    /**
     * The node only has a left son
     */
    private static final int ONLY_HAS_LEFT_SON = -1;

    /**
     * The node has two sons
     */
    private static final int HAS_BOTH_SONS = 2;

    /**
     * The node is a right son of its father
     */
    private static final int IS_RIGHT_SON = 1;

    /**
     * The node is a left son of its father
     */
    private static final int IS_LEFT_SON = -1;

    /**
     * The "depth" of null.
     */
    private static final int NULL_DEPTH = -1;


    /*----=  Constructors  =-----*/

    /**
     * Default constructor
     */
    public BinarySearchTree() {
        root = null;
        size = 0;
    }

    /**
     * A constructor that builds the tree by adding the elements in the input array one-by-one If the same
     * values appears twice (or more) in the list, it is ignored.
     *
     * @param data values to add to tree
     */
    public BinarySearchTree(int[] data) {
        if (data != null) {
            for (int num : data) {
//            try {
                add(num);

//            }
//            catch (Exception) //TODO figure out how to add an exception here
            }
        }
    }

    /**
     * A copy-constructor that builds the tree from existing tree
     *
     * @param tree tree to be copied
     */
    public BinarySearchTree(BinarySearchTree tree) {
        for (Integer num : tree) {
            add(num);
        }
    }


    /*----=  Basic tree operation methods  =-----*/

    /**
     * Add a new node with the given key to the tree.
     *
     * @param newValue the value of the new node to add.
     * @return true if the value to add is not already in the tree and it was successfully added, false otherwise.
     */
    public boolean add(int newValue) {
        if (root == null) {
            root = new Node(newValue, null);
            fixTreeAfterChange(root);
            size++;
            return true;
        }

        // Node should be added as the son of the last node that was found by contain.
        if (contains(newValue) == NOT_CONTAIN) {

            Node newNode = new Node(newValue, lastNodeFound);

            if (newValue > lastNodeFound.getValue()) {
                lastNodeFound.setRightSon(newNode);

            } else {
                lastNodeFound.setLeftSon(newNode);
            }
            size++;
            updateHeight(newNode);
            fixTreeAfterChange(lastNodeFound);
            return true;
        }
        return false;
    }

    /**
     * Check whether the tree contains the given input value.
     *
     * @param searchVal value to search for
     * @return if val is found in the tree, return the depth of the node (0 for the root) with the given
     * value if it was found in the tree, -1 otherwise.
     */
    public int contains(int searchVal) {
        if (root == null) {
            return NOT_CONTAIN;
        }
        return containsHelper(searchVal, root);
    }

    /**
     * @param searchVal value to search for
     * @param curNode   the node that is currently being checked
     * @return if val is found in the tree, return the depth of the node (0 for the root) with the given
     * value if it was found in the tree, -1 otherwise.
     */
    private int containsHelper(int searchVal, Node curNode) {

        if (searchVal == curNode.getValue()) {
            lastNodeFound = curNode;
            return curNode.getDepth();

        } else if (searchVal > curNode.getValue() && curNode.getRightSon() != null) {
            return containsHelper(searchVal, curNode.getRightSon());

        } else if (searchVal < curNode.getValue() && curNode.getLeftSon() != null) {
            return containsHelper(searchVal, curNode.getLeftSon());

        } else {
            lastNodeFound = curNode;
            return NOT_CONTAIN;
        }
    }


    /**
     * Remove a node from the tree, if it exists.
     *
     * @param toDelete value to delete
     * @return true if toDelete found and deleted
     */
    public boolean delete(int toDelete) {
        // item not in tree
        if (contains(toDelete) == NOT_CONTAIN) {
            return false;
        }
        //item in tree
        deleteHelper();
        size--;
        return true;

    }

    /**
     * @return the number of nodes in the tree
     */
    public int size() {
        return size;
    }

    /**
     * @return an iterator for the BST Tree. The returned iterator iterates over the tree nodes in an
     * ascending order, and does NOT implement the remove() method.
     */
    public java.util.Iterator<Integer> iterator() {

        Iterator<Integer> integerIterator = new Iterator<Integer>() { //TODO should maybe be done the funny way I didn't understand

            Node currNode = findSmallestDescendant(root);


            public boolean hasNext() {
                return (currNode != null);
            }

            public Integer next() {
                Integer nodeValue = currNode.getValue();
                currNode = findSuccessor(currNode);
                return nodeValue;
            }
            //TODO add an override for the remove() function which will throw an exception
        };

        return integerIterator;
    }

    /*----=  delete helper methods  =-----*/

    private void deleteHelper() {

        Node nodeToDelete = lastNodeFound;

        int numOfSons = numOfSons(nodeToDelete);
        Node father = nodeToDelete.getFather();

        if (father == null) {
            deleteRoot(numOfSons, nodeToDelete);

        } else {

            int sonPosition = getSonPosition(nodeToDelete);

            switch (numOfSons) {
                case HAS_NO_SONS:
                    deleteNodeWithNoSon(sonPosition, father);
                    break;

                case ONLY_HAS_RIGHT_SON:
                    deleteNodeWithRightSon(sonPosition, nodeToDelete, father);
                    break;

                case ONLY_HAS_LEFT_SON:
                    deleteNodeWithLeftSon(sonPosition, nodeToDelete, father);
                    break;

                case HAS_BOTH_SONS:
                    deleteNodeWithTwoSons(nodeToDelete);
                    break;
            }
            updateHeight(father);
            fixTreeAfterChange(father);
        }
    }


    /**
     * Deletes a node that has no sons
     *
     * @param sonPosition an int representing if the node is a right son or a left son of his father
     * @param father      the node's father
     */
    private void deleteNodeWithNoSon(int sonPosition, Node father) {
        if (sonPosition == IS_RIGHT_SON) {
            father.setRightSon(null);
        } else {
            father.setLeftSon(null);
        }

    }

    /**
     * Deletes a node that has only a right son
     *
     * @param sonPosition an int representing if the node is a right son or a left son of his father
     * @param father      the node's father
     */
    private void deleteNodeWithRightSon(int sonPosition, Node nodeToDelete, Node father) {
        if (sonPosition == IS_RIGHT_SON) {
            father.setRightSon(nodeToDelete.getRightSon());
        } else { //son position is left
            father.setLeftSon(nodeToDelete.getRightSon());
        }
        nodeToDelete.getRightSon().setFather(father);
        updateDepth(nodeToDelete.getRightSon());
    }

    /**
     * Deletes a node that has only a left son
     *
     * @param sonPosition an int representing if the node is a right son or a left son of his father
     * @param father      the node's father
     */
    private void deleteNodeWithLeftSon(int sonPosition, Node nodeToDelete, Node father) {
        if (sonPosition == IS_RIGHT_SON) {
            father.setRightSon(nodeToDelete.getLeftSon());
        } else { //son position is left
            father.setLeftSon(nodeToDelete.getLeftSon());
        }
        nodeToDelete.getLeftSon().setFather(father);
        updateDepth(nodeToDelete.getLeftSon());
    }

    /**
     * @param nodeToDelete the node to be delete
     *                     Deletes a node that has two sons by copying the value of its successor to the nodes' value.
     */
    private void deleteNodeWithTwoSons(Node nodeToDelete) {
        Node newRootValNode = findSuccessor(nodeToDelete);

        int newData = newRootValNode.getValue();
        nodeToDelete.setValue(newData);

        int newNodePosition = getSonPosition(newRootValNode);
        Node newRootValNodeRight = newRootValNode.getRightSon(); // either null or a node
        if (newRootValNodeRight != null) {
            newRootValNodeRight.setFather(newRootValNode.getFather());
        }
        if (newNodePosition == IS_RIGHT_SON) {
            newRootValNode.getFather().setRightSon(newRootValNodeRight);
        } else { // is left son
            newRootValNode.getFather().setLeftSon(newRootValNodeRight);
        }
        fixTreeAfterChange(newRootValNode.getFather());
        updateHeight(newRootValNode.getFather());
        if (newRootValNodeRight != null) {
            updateDepth(newRootValNodeRight);
        }
    }

    /**
     * deletes the root node of a tree
     *
     * @param numOfSons the number of sons the node has (0 - if no sons, 1 - if only has a right son, -1 - if only
     *                  has a left son, 2 - if has two sons)
     */
    private void deleteRoot(int numOfSons, Node nodeToDelete) {
        switch (numOfSons) {
            case HAS_NO_SONS:
                root = null;
                break;

            case ONLY_HAS_RIGHT_SON:
                nodeToDelete.getRightSon().setFather(null);
                root = nodeToDelete.getRightSon();
                updateDepth(root);
                break;

            case ONLY_HAS_LEFT_SON:
                nodeToDelete.getLeftSon().setFather(null);
                root = nodeToDelete.getLeftSon();
                updateDepth(root);
                break;

            case HAS_BOTH_SONS:
                deleteNodeWithTwoSons(nodeToDelete);
                break;
        }
    }

    /*----=  general helper methods  =-----*/


    /**
     * Finds the smallest descendant of a given node
     *
     * @param node the node for whom we want to find a descendant
     * @return the smallest descendant of the node
     */
    private Node findSmallestDescendant(Node node) {
        Node currNode = node;
        while (currNode != null && currNode.getLeftSon() != null) {
            currNode = currNode.getLeftSon();
        }
        return currNode;
    }

    /**
     * Returns the number of sons a node has
     *
     * @param node the node being checked
     * @return the number of sons the node has (0 - if no sons, 1 - if only has a right son, -1 - if only
     * has a left son, 2 - if has two sons)
     */
    private int numOfSons(Node node) {
        if (node.getLeftSon() == null && node.getRightSon() == null) {
            return HAS_NO_SONS;
        } else if (node.getRightSon() != null && node.getLeftSon() == null) {
            return ONLY_HAS_RIGHT_SON;
        } else if (node.getRightSon() == null && node.getLeftSon() != null) {
            return ONLY_HAS_LEFT_SON;
        } else {
            return HAS_BOTH_SONS;
        }
    }

    /**
     * Returns if the given node is a right son or a left son of his father
     *
     * @param node the node being checked
     * @return 1 if the son is the right son of his father, -1 if the son is the left son of his father.
     */
    private int getSonPosition(Node node) {
        if (node.getValue() >= node.getFather().getValue()) { //the "equals" is in the case that the
            // smallest descendant is the node's right son.
            return IS_RIGHT_SON;
        } else {
            return IS_LEFT_SON;
        }
    }

    private Node findSuccessor(Node node) {
        // the node has a right child
        if (node.getRightSon() != null) {
            return findSmallestDescendant(node.getRightSon());
        }
        // the node doesn't have a right child
        Node father = node.getFather();

        while (father != null && getSonPosition(node) == IS_RIGHT_SON) {
            node = father;
            father = node.getFather();
        }
        return father;
    }

    /**
     * Updates the height of a node by recursively calling for the height of the node's children, until the
     * end of the tree is reached (a leaf has a height of 0.) Then, it takes the maximum from its left and
     * right sub-trees and adds 1 to get the node's height.
     *
     * @param node the node whose height we are updating
     */
    void updateHeight(Node node) {
        Node curNode = node;

        while (curNode != null) {
            int leftHeight = 0;
            int rightHeight = 0;

            if (curNode.getLeftSon() != null) {
                leftHeight = curNode.getLeftSon().getHeight();
            }
            if (curNode.getRightSon() != null) {
                rightHeight = curNode.getRightSon().getHeight();
            }
            if (numOfSons(curNode) != HAS_NO_SONS) {
                curNode.setHeight(Math.max(leftHeight, rightHeight) + 1);
            } else { // node is a leaf with a height 0
                curNode.setHeight(Math.max(leftHeight, rightHeight));
            }

            curNode = curNode.getFather();
        }
    }

    /**
     * Updates the depth of a node, which is the distance from the root to the node. The depth is a root is 0.
     * @param node the node who's depth we are updating (and all of his children't depths - recursively)
     */
    void updateDepth(Node node) {
        if (node != null) {
            int fatherDepth = NULL_DEPTH;
            if (node.getFather() != null) {
                fatherDepth = node.getFather().getDepth();
            }
            node.setDepth(fatherDepth + 1);
            updateDepth(node.getRightSon());
            updateDepth(node.getLeftSon());
        }
    }

    /**
     * If the tree we are working with is a type of tree that inherits from a BST, it may need to
     * have more modifications after adding or deleting a node. The other classes can then override this
     * method, and by getting the node that was changed, they can complete the modifications that are
     * unique to that tree. (For example, in an AVL tree, this function allows the tree to be
     * rebalanced, as needed.)
     *
     * @param node the node that around it might need to be changed
     */
    void fixTreeAfterChange(Node node) {

    }

    /*----=  delete before submission methods  =-----*/

    /**
     * This method gets a value and returns the node who has that value
     * This method is mostly used for our testing of the code - since most of the functions call for a
     * Node, but in our diagrams we have numbers, this code will return the node with the value we want.
     *
     * @param nodeValue the value of the node we want
     * @return the node with the value we were looking for
     */
    public Node returnNode(int nodeValue) {
        contains(nodeValue);
        return lastNodeFound;
    }

}
