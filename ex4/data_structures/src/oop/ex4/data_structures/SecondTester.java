package oop.ex4.data_structures;

import sun.security.x509.AVA;

public class SecondTester {
    public static void main(String[] args) {
//        testConstructorOnlyWithNum();
//        AvlTree avlTree = rotatingLeftTest();
//        rotateRightThenLeft(avlTree);
//        AvlTree avlTree = rotatingRightTest();
//        rotateLeftandThenRight(avlTree);
//        copyTreeConstructor();
        copyAvlTreeConstructor();

    }


    private static void testConstructorOnlyWithNum() {

        int[] dataSet = new int[7];
        dataSet[0] = 5;
        dataSet[1] = 8;
        dataSet[2] = 15;
        dataSet[3] = 2;
        dataSet[4] = 3;
        dataSet[5] = 9;
        dataSet[6] = 7;

        BinarySearchTree constructTree = new BinarySearchTree(dataSet);

        System.out.println("Tests A");
        System.out.println(constructTree.root.getValue() == 5);
        System.out.println(constructTree.root.getLeftSon().getValue() == 2);
        System.out.println(constructTree.root.getRightSon().getValue() == 8);
        System.out.println(constructTree.returnNode(15).getFather().getValue() == 8);
        System.out.println(constructTree.returnNode(15).getRightSon() == null);
        System.out.println(constructTree.returnNode(15).getLeftSon().getValue() == 9);

//        constructTree.add(2);
//        constructTree.add(3);
//        constructTree.add(9);
//        constructTree.add(7);

        System.out.println("\n Tests B");
        System.out.println(constructTree.returnNode(3).getHeight() == 0);
        System.out.println(constructTree.returnNode(2).getRightSon().getValue() == 3);
        System.out.println(constructTree.returnNode(2).getLeftSon() == null);
        System.out.println(constructTree.returnNode(9).getFather().getValue() == 15);
        System.out.println(constructTree.returnNode(7).getFather().getValue() == 8);

    }


    private static void copyTreeConstructor() {

        //Create binary tree to copy
        int[] dataSet = new int[7];

        dataSet[0] = 5;
        dataSet[1] = 8;
        dataSet[2] = 15;
        dataSet[3] = 2;
        dataSet[4] = 3;
        dataSet[5] = 9;
        dataSet[6] = 7;

        BinarySearchTree constructTree = new BinarySearchTree(dataSet);
//        java.util.Iterator<Integer> itr = constructTree.iterator();
//        while (itr.hasNext()) {
//            System.out.println(itr.next());
//        }

        BinarySearchTree newCopyTree = new BinarySearchTree(constructTree);

        System.out.println("Tests A");
        System.out.println(newCopyTree.root.getValue() == 2);
        System.out.println(newCopyTree.root.getLeftSon() == null);
        System.out.println(newCopyTree.root.getRightSon().getValue() == 3);
        System.out.println(newCopyTree.returnNode(15).getFather().getValue() == 9);
        System.out.println(newCopyTree.returnNode(15).getRightSon() == null);
        System.out.println(newCopyTree.returnNode(15).getLeftSon() == null);


    }
    private static void copyAvlTreeConstructor() {

        //Create binary tree to copy
        int[] dataSet = new int[7];

        dataSet[0] = 5;
        dataSet[1] = 8;
        dataSet[2] = 15;
        dataSet[3] = 2;
        dataSet[4] = 3;
        dataSet[5] = 9;
        dataSet[6] = 7;

        AvlTree constructTree = new AvlTree(dataSet);

        System.out.println(constructTree.root.getValue());
//        java.util.Iterator<Integer> itr = constructTree.iterator();
//        while (itr.hasNext()) {
//            System.out.println(itr.next());
//        }

        AvlTree newCopyTree = new AvlTree(constructTree);

        System.out.println("AVL constructor from existing tree");
        System.out.println("Tests A - Checking values");
        System.out.println(newCopyTree.root.getValue() == 7);
        System.out.println(newCopyTree.root.getLeftSon().getValue() == 3);
        System.out.println(newCopyTree.root.getRightSon().getValue() == 9);
        System.out.println(newCopyTree.returnNode(15).getFather().getValue() == 9);
        System.out.println(newCopyTree.returnNode(15).getRightSon() == null);
        System.out.println(newCopyTree.returnNode(15).getLeftSon() == null);
        System.out.println(newCopyTree.returnNode(3).getRightSon().getValue() == 5);
        System.out.println(newCopyTree.returnNode(3).getLeftSon().getValue() == 2);

        System.out.println("\n Test B - Checking heights");
        System.out.println(newCopyTree.returnNode(7).getHeight() == 2);
        System.out.println(newCopyTree.returnNode(2).getHeight() == 0);
        System.out.println(newCopyTree.returnNode(8).getHeight() == 0);





    }

    private static AvlTree rotatingLeftTest() {
        System.out.println("\n *********Rotating left test*********");
        AvlTree avlTree = new AvlTree();

        System.out.println("\n adding 5");

        avlTree.add(5);

        System.out.println(avlTree.root.getValue() == 5);
        System.out.println(avlTree.root.getHeight() == 0);

        System.out.println("\n adding 8");

        avlTree.add(8);

        System.out.println("\n No need to rotate");
        System.out.println(avlTree.root.getRightSon().getValue() == 8);
        System.out.println(avlTree.returnNode(8).getHeight() == 0);

        System.out.println("\n adding 10");
        avlTree.add(10);

        System.out.println("\n Need to rotate left");
        System.out.println(avlTree.root.getLeftSon().getValue() == 5);
        System.out.println(avlTree.returnNode(5).getRightSon() == null);
        System.out.println(avlTree.root.getRightSon().getValue() == 10);
        System.out.println(avlTree.root.getValue() == 8);
        System.out.println(avlTree.root.getHeight() == 1);

        avlTree.add(20);
        avlTree.add(30);

        System.out.println("\n Adding 30 - Need to rotate left");
        System.out.println(avlTree.root.getValue() == 8);
        System.out.println(avlTree.root.getHeight() == 2);
        System.out.println(avlTree.root.getRightSon().getValue() == 20);
        System.out.println(avlTree.returnNode(20).getRightSon().getValue() == 30);
        System.out.println(avlTree.returnNode(20).getLeftSon().getValue() == 10);
        System.out.println(avlTree.returnNode(10).getFather().getValue() == 20);
        System.out.println(avlTree.returnNode(10).getHeight() == 0);
        System.out.println(avlTree.returnNode(20).getHeight() == 1);

        avlTree.add(25);
        System.out.println("\n Adding 25 - need to rotate left");
        System.out.println("Testing values");
        System.out.println(avlTree.root.getValue() == 20);
        System.out.println(avlTree.returnNode(20).getFather() == null);
        System.out.println(avlTree.returnNode(8).getRightSon().getValue() == 10);
        System.out.println(avlTree.returnNode(8).getLeftSon().getValue() == 5);
        System.out.println(avlTree.returnNode(20).getLeftSon().getValue() == 8);
        System.out.println(avlTree.returnNode(8).getFather().getValue() == 20);
        System.out.println(avlTree.returnNode(25).getFather().getValue() == 30);
        System.out.println(avlTree.returnNode(30).getRightSon() == null);

        System.out.println("testing heights");
        System.out.println(avlTree.root.getHeight() == 2);
        System.out.println(avlTree.returnNode(30).getHeight() == 1);
        System.out.println(avlTree.returnNode(25).getHeight() == 0);
        System.out.println(avlTree.returnNode(8).getHeight() == 1);
        System.out.println(avlTree.returnNode(10).getHeight() == 0);

        return avlTree;

    }

    private static AvlTree rotatingRightTest() {
        System.out.println("\n *********Rotating right test*********");
        AvlTree avlTree = new AvlTree();

        avlTree.add(17);
        avlTree.add(14);

        System.out.println("\n Adding 14 - no need to rotate");
        System.out.println("checking values");
        System.out.println(avlTree.root.getValue() == 17);
        System.out.println(avlTree.root.getLeftSon().getValue() == 14);
        System.out.println("checking heights");
        System.out.println(avlTree.root.getHeight() == 1);
        System.out.println(avlTree.root.getLeftSon().getHeight() == 0);

        System.out.println("\n adding 11 - need to rotate");
        avlTree.add(11);
        System.out.println(avlTree.root.getValue() == 14);
        System.out.println(avlTree.root.getRightSon().getValue() == 17);
        System.out.println(avlTree.root.getLeftSon().getValue() == 11);
        System.out.println(avlTree.returnNode(14).getFather() == null);
        System.out.println(avlTree.returnNode(17).getFather().getValue() == 14);

        System.out.println("\n adding 8 - no need to rotate");
        avlTree.add(8);
        System.out.println("checking values");
        System.out.println(avlTree.root.getValue() == 14);
        System.out.println(avlTree.returnNode(8).getFather().getValue() == 11);
        System.out.println(avlTree.returnNode(11).getLeftSon().getValue() == 8);
        System.out.println("checking heights");
        System.out.println(avlTree.root.getHeight() == 2);

        System.out.println("\n adding 6 - need to rotate");
        avlTree.add(6);

        System.out.println("checking values");
        System.out.println(avlTree.root.getValue() == 14);
        System.out.println(avlTree.returnNode(8).getFather().getValue() == 14);
        System.out.println(avlTree.returnNode(11).getLeftSon() == null);
        System.out.println(avlTree.root.getLeftSon().getValue() == 8);
        System.out.println(avlTree.returnNode(8).getLeftSon().getValue() == 6);
        System.out.println(avlTree.returnNode(8).getRightSon().getValue() == 11);

        System.out.println(avlTree.returnNode(11).getLeftSon() == null);

        System.out.println("checking heights");

        System.out.println(avlTree.returnNode(14).getHeight() == 2);
        System.out.println(avlTree.returnNode(6).getHeight() == 0);
        System.out.println(avlTree.returnNode(8).getHeight() == 1);


        return avlTree;
    }

    private static void rotateLeftandThenRight(AvlTree avlTree) {
        System.out.println(" \n ***** Rotate left and then rotate right *****");

        System.out.println("\n Adding 9 - need ro rotate left and then right");
        avlTree.add(9);

        System.out.println("Checking updated children correctly");
        System.out.println(avlTree.root.getValue() == 11);
        System.out.println(avlTree.root.getRightSon().getValue() == 14);
        System.out.println(avlTree.root.getLeftSon().getValue() == 8);
        System.out.println(avlTree.returnNode(8).getRightSon().getValue() == 9);


        System.out.println("Checking updated fathers correctly");
        System.out.println(avlTree.returnNode(9).getFather().getValue() == 8);
        System.out.println(avlTree.returnNode(8).getFather().getValue() == 11);
        System.out.println(avlTree.returnNode(14).getFather().getValue() == 11);
        System.out.println(avlTree.returnNode(17).getFather().getValue() == 14);
        System.out.println(avlTree.returnNode(11).getFather() == null);


    }

    private static void rotateRightThenLeft(AvlTree avlTree) {
        System.out.println("\n ******Rotate Right then Left***");

        avlTree.add(15);
        avlTree.add(13);

        System.out.println("Checking values - children");
        System.out.println(avlTree.returnNode(8).getRightSon().getValue() == 13);
        System.out.println(avlTree.returnNode(13).getRightSon().getValue() == 15);
        System.out.println(avlTree.returnNode(13).getLeftSon().getValue() == 10);

        System.out.println("checking values - fathers");
        System.out.println(avlTree.returnNode(10).getFather().getValue() == 13);
        System.out.println(avlTree.returnNode(13).getFather().getValue() == 8);
        System.out.println(avlTree.returnNode(15).getFather().getValue() == 13);

        System.out.println("checking heights");
        System.out.println(avlTree.returnNode(10).getHeight() == 0);
        System.out.println(avlTree.returnNode(15).getHeight() == 0);
        System.out.println(avlTree.returnNode(13).getHeight() == 1);
        System.out.println(avlTree.returnNode(8).getHeight() == 2);
        System.out.println(avlTree.returnNode(20).getHeight() == 3);


    }

}
